#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import pathlib

import utils


def main(**kwargs):
    path = pathlib.Path("~/.local/share/ProSlave/")
    data = {}
    with path.joinpath("templates/templates.json").expanduser().open("r") as file:
        data = json.load(file)
    chosen_template = utils.select_from_list("Choose a project template", list(data.keys()), list(data.values()))
    template_data = utils.get_json_data_from_template(path.joinpath("templates"), data.keys[chosen_template])


if __name__ == "__main__":
    main()
