# ProjectSlave
This project aims to be a generator and manager for programming projects. 
First focus lies on C/C++ since this is my main language but it aims to be generic enough to 
support any language or project format.

I don't plan on making this some kind of terminal IDE or even compete with any IDEs. I intend this tool to provide
management and automation of tedious and repetitive tasks managing your projects.  
Maybe you just start out and are scared by all the new stuff or you are afraid to make mistakes or you are tired of 
Typing out basic structure the 10 millionth CMakeLists.txt... Simply let the virtual slave type it out for you.

## Planned Features
* [ ] Platform independence
* [ ] Generation of directory structure for the project
* [ ] Generation for config files, build scripts, ...
* [ ] Package manager interface (conan, pip, ...)
* [ ] Project templates
    * [ ] Project templates are modular
    * [ ] File templates get a separate index
    * [ ] File templates get universal or modular   
* [ ] Generated projects are platform independent by default until you decide breaking platorm independence
* [ ] Import of existing Projects
* [ ] config file to store project settings (never should be touched by user)
* [ ] template repository ???
* [ ] integration with version control  

### Templates
Templates define a directory structure and all necessary scripts and config files. Template definitions of config 
files or scripts should be managed in a directory by them selfes or should be modular enough so that everyone can 
contribute his/her prefered modules.

## My preferred tools
I often use the same tools and will define my default toolchain that will be used in my template.  
1. Git
2. Cmake
3. Conan
4. Doxygen
5. Ninja
6. Google test

If you start a new project with this tool it will initialize a git repository and automatically commit
the generated files. (This feature is not jet implemented)  

CMake is one of the most powerful, widely adopted and versatile buildsystem generator. My biggest gripe with CMake is 
its strange, "shelly" syntax. I would like to avoid writing lots of cmake scripts so this tool should 
do ideally all but realistically most of it.

Conan is a package manager made for C/C++. It will serve to save on build time and installing headache.

Doxygen generates documentation from sourcecode and special comments. most IDEs also understand doxygen comments. 
This tool should include it into the build script.

Ninja is a buildsystem with focus on speed. this tool should setup everything that ninja can be used.

At the moment the only testing framework I ever used was google test and it's concept makes sense for me. The 
tool should offer to generate testing boilerplate for everything you create using the wizard.

If you use the config wizard to create a library or class i expect the wizard to generate all the boilerplate and 
repetitive things by itself. Like it generates a cpp file containting implementations for a created header.

