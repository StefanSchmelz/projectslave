#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import pathlib
import re


def get_json_files_in_directory(p):
    return [str(item).split("/")[-1][:-5] for item in p.expanduser().glob("*.json")]


def select_from_list(prompt, entries, description=[]):
    valid = False
    choice = -1
    while not valid:
        print(prompt)
        for index, item in enumerate(entries):
            print("    {}. {}".format(index + 1, item))
            try:
                print("        {}".format(description[index]))
            except IndexError:
                pass
        try:
            choice = int(input("> ")) - 1
        except ValueError:
            pass
        if len(entries) > choice > -1:
            valid = True
    return choice


def get_json_data_from_template(p, name):
    with p.joinpath(name + ".json").expanduser().open() as file:
        data = json.load(file)
    return data


def replace_template_var(lines, vars={}):
    pattern = re.compile(r"@[A-Z0-9]+@")
    result = []
    for line in lines:
        for item in pattern.finditer(line):
            result.append(line.replace(item.group(), vars[item.group().strip("@")]))
    return result


def exclude_templates_file(files):
    if "templates" in files:
        files.remove("templates")
    return files


def main(**kwargs):
    p = pathlib.Path("~/.local/share/ProSlave/templates")
    select_from_list("Select a Template:", get_json_files_in_directory(p))

    vars = {"TEST": "Hallo Welt",
            "VERSION": "0.1",
            "DESCRIPTION": "This is just a test property!"}
    lines = ["Dieser Test soll \"@TEST@\" ausgeben.",
             "Dieser Test hat die Version @VERSION@",
             "Dieser Test ist beschrieben mit: \"@DESCRIPTION@\"."]
    for line in replace_template_var(lines, vars):
        print(line)


if __name__ == "__main__":
    main()
