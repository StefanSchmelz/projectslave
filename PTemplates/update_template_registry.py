#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import pathlib


def get_json_files_in_directory(p):
    return [str(item).split("/")[-1][:-5] for item in p.expanduser().glob("*.json")]


def main(**kwargs):
    p = pathlib.Path("~/.local/share/ProSlave/templates")
    files = get_json_files_in_directory(p)
    if "templates" in files:
        files.remove("templates")
    templates = {}
    for f in files:
        with p.joinpath(f + ".json").expanduser().open() as file:
            data = json.load(file)
            templates[f] = data["description"]
    with p.joinpath("templates.json").expanduser().open("w") as file:
        json.dump(templates, file, indent=2)
    print("Done")


if __name__ == "__main__":
    main()
