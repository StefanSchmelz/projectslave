#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re


class TemplateFiller:
    def __init__(self, input_data, output_data, vars={}, autoSysInfo=True):
        self.data = vars
        self.template = input_data
        self.output = output_data
        self.contents = []
        self.pattern = re.compile(r"@[A-Z0-9]+@")

    def load(self):
        with open(self.template, "r") as file:
            lines = file.readlines()
            for line in lines:
                for item in self.pattern.finditer(line):
                    print(item.group().strip("@"))


def main(**kwargs):
    i = TemplateFiller("/home/stefan/.local/share/ProSlave/basic.json", "")
    i.load()


if __name__ == "__main__":
    main()
