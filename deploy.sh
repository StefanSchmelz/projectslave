#!/usr/bin/env bash

echo "Cleaning the Project path ..."
if [[ ! -d "~/.local/share/ProSlave/templates" ]]; then
    mkdir -p ~/.local/share/ProSlave/templates
else
    rm -rf ~/.local/share/ProSlave/
fi

echo "Copying templates from Project dir ..."
cp -r $(pwd)/PTemplates/*.json ~/.local/share/ProSlave/templates/

echo "Copying template library update script from project dir ..."
if [[ ! -x $(pwd)/PTemplates/update_template_registry.py ]]; then chmod +x $(pwd)/PTemplates/update_template_registry.py; fi
cp $(pwd)/PTemplates/update_template_registry.py ~/.local/share/ProSlave/update_template_registry.py

echo "Updating templates ..."
$(pwd)/PTemplates/update_template_registry.py
