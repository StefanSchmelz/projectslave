#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import pathlib


def get_json_files_in_directory(p):
    return [str(item).split("/")[-1][:-5] for item in p.expanduser().glob("*.json")]


def main(**kwargs):
    p = pathlib.Path("~/.local/share/ProSlave/templates")
    templates = get_json_files_in_directory(p)
    if "templates" in templates:
        templates.remove("templates")
    print(templates)
    print("Select a template:")
    for index, item in enumerate(templates):
        print("   {} - {}".format(index + 1, item))
        if p.joinpath(item + ".json").expanduser().exists():
            with p.joinpath(item + ".json").expanduser().open() as file:
                data = json.load(file)
                print("       {}".format(data["description"]))
                print("       directories:")
                for dir in data["directories"]:
                    print("           - {}".format(dir))
                print("       CMAKE components:")
                for segment in data["files"][0]["cmake"]:
                    print("           {} - {}".format(segment["name"], segment["description"]))


if __name__ == "__main__":
    main()
